##
# PHP Image based on PHP's Alpine official image
# NGINX + PHP-FPM Multi-Stage Dockerfile
# PHP 8.3
##

FROM php:8.3-fpm-alpine AS base

ENV NPM_VERSION=10

USER root
WORKDIR /tmp

COPY ./scripts/packages-base-stage.sh /tmp/packages.sh

# Install dependencies
RUN chmod +x /tmp/*.sh \
    && sh /tmp/packages.sh

# Add docker-php-extension-installer script
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

# Install PHP extensions
RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions \
    bcmath \
    calendar \
    exif \
    gd \
    gmp \
    igbinary \
    imagick/imagick@master \
    intl \
    memcached \
    msgpack \
    mysqli \
    pdo_mysql \
    pdo_pgsql \
    pcntl \
    redis-stable \
    soap \
    uuid \
    xmlrpc \
    xdebug-stable \
    zip

# Composer installer
COPY scripts/composer-setup.sh /tmp/composer-install.sh
RUN chmod +x /tmp/composer-install.sh && \
    sh /tmp/composer-install.sh && \
    mv /tmp/composer.phar /usr/local/bin/composer && \
    rm /tmp/composer-install.sh

# Force NPM version and INSTALL Yarn
RUN npm i -g --force npm@${NPM_VERSION} && \
    curl -o- -L https://yarnpkg.com/install.sh | bash

# Final stage: Get what's needed from previous stages and add config files
FROM php:8.3-fpm-alpine AS final

ENV IMAGE_USER=php
ENV HOME=/home/$IMAGE_USER
ENV COMPOSER_HOME=$HOME/.composer

# Install packages on the final layer
COPY ./scripts/packages-final-stage.sh /tmp/packages.sh
COPY ./scripts/cleanup.sh /tmp/cleanup.sh

RUN chmod +x /tmp/*.sh && /tmp/packages.sh

RUN apk --update --no-cache add nginx

RUN /tmp/cleanup.sh

# Extensions & config
COPY --from=base /usr/local/lib/php/ /usr/local/lib/php/
COPY --from=base /usr/local/etc/ /usr/local/etc/
# Composer binary
COPY --from=base /usr/local/bin/composer /usr/local/bin/composer
# NPM & Yarn
COPY --from=base /usr/local/lib/node_modules/ /usr/local/lib/node_modules/
RUN ln -s ../lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm
RUN ln -s ../lib/node_modules/npm/bin/npx-cli.js /usr/local/bin/npx
COPY --from=base /root/.yarn $HOME/.yarn

## NGINX
COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/nginx-default.conf /etc/nginx/conf.d/default.conf
COPY default/index.html /var/www/app/
COPY default/index.php /var/www/app/

## PHP
COPY php/xdebug.ini /usr/local/etc/php/conf.d/20-xdebug.ini
COPY php/extra-conf.ini /usr/local/etc/php/conf.d/zz-conf.ini
COPY php/www-nginx.conf /usr/local/etc/php-fpm.d/zz-docker.conf

## SUPERVISOR
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

## DOTFILES
COPY dotfiles/.bash_profile $HOME/.bash_profile
COPY dotfiles/.bashrc $HOME/.bashrc
COPY dotfiles/.vimrc $HOME/.vimrc

# ADD NON ROOT USER
RUN adduser --disabled-password --gecos "" $IMAGE_USER \
    && echo "$IMAGE_USER  ALL = ( ALL ) NOPASSWD: ALL" >> /etc/sudoers \
    && mkdir -p /var/www/app \
    && chown -R $IMAGE_USER:$IMAGE_USER /var/www $HOME /run /var/lib/nginx /var/log/nginx \
    && mkdir -p $COMPOSER_HOME && chown -R $IMAGE_USER:$IMAGE_USER $COMPOSER_HOME

WORKDIR /var/www/app

# Add folders to the PATH.
ENV PATH=/usr/local/bin:$HOME/.composer/vendor/bin:./vendor/bin:$HOME/.yarn/bin:$PATH

USER $IMAGE_USER

EXPOSE 80

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
