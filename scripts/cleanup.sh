#!/usr/bin/env sh
rm -rf ~/.composer/cache/* \
    /root/.npm /root/.node-gyp /root/.gnupg \
    /tmp/* \
    /var/cache/apk/* \
    /usr/includes/* \
    /usr/lib/node_modules/npm/man \
    /usr/lib/node_modules/npm/doc \
    /usr/lib/node_modules/npm/html \
    /usr/lib/node_modules/npm/scripts \
    /usr/share/man/* \
    /var/tmp/*
