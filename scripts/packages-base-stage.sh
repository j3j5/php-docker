#!/usr/bin/env sh

apk --update --no-cache add \
    autoconf \
    bash \
    build-base \
    ca-certificates \
    curl \
    file \
    freetype \
    freetype-dev \
    g++ \
    gcc \
    git \
    gpg \
    gpg-agent \
    icu-dev \
    icu-libs \
    imagemagick-libs \
    libavif \
    libc-dev \
    libgomp \
    libjpeg \
    libmemcached \
    libpng \
    libuuid \
    libwebp \
    libxpm \
    libzip \
    libzip-dev \
    make \
    nodejs \
    npm \
    oniguruma-dev \
    openssh-client \
    openssl \
    openssl-dev \
    patch \
    postgresql-libs \
    python3 \
    tar \
    xz \
    zip \
    zlib \
    zlib-dev

